import './globals.css'
import { Providers } from './providers'

export const metadata = {
  title: 'โครงการเพื่อนแบ่งปันเพื่อน',
  description: ''
}

export default function RootLayout({
  children
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body>
        <Providers>{children}</Providers>
      </body>
    </html>
  )
}
